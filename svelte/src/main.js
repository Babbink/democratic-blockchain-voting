import App from "./App.svelte";
import { Web3 } from "svelte-web3";
import VoteContract from "./contracts/Vote.json";

const name = "TEST";
const contractAddress = "0x95F2dF4534B1B2AEE43f700eFa2f5253cE03981B";

const provider = window.ethereum || "HTTP://127.0.0.1:8545";
const web3 = new Web3(provider);
const contract = new web3.eth.Contract(VoteContract.abi, contractAddress);

const app = new App({
  target: document.body,
  props: {
    name,
    contract,
    provider,
    web3,
  },
});

export default app;
