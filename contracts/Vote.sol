// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract Vote {
    address public chairman; // Contractowner
    uint public votingPowerNonAuthorized = 0;
    uint public votingPowerAuthorized = 1;
    enum State { Open, Closed }

    struct RegisteredPerson {
        uint validUntilTimeStamp;
    }

    struct Proposal {
        string title; // proposal title
        string description; // proposal description
        string[] pros;
        string[] cons;
        uint forVotes; // votes in favor
        uint againstVotes; // votes against
        uint voteCount; // total amount of votes
        State state;
    }

    Proposal[] public proposals; // list with all the proposals
    mapping(uint => mapping(address => bool)) public hasVoted; // bool of every proposals voters
    mapping(address => RegisteredPerson) public RegisteredVoters;
    mapping(address => RegisteredPerson) public Foremen;

    // Check if the sender is the manager
    modifier managerOnly(address sender) {
        require(sender == chairman, "Only the manager can perform this action.");
        _;
    }

    // Check if the sender is registered valid foremen
    modifier foremenOnly(address sender) {
        require(Foremen[sender].validUntilTimeStamp >= block.timestamp, "Only foremen can perform this action.");
        _;
    }

    // Check if the proposal exists
    modifier proposalExists(uint index) {
        require(index <= proposals.length && proposals.length > 0, "The proposal does not exist");
        _;
    }

    // Set the creator to the chairman and register him as a foreman for a year
    constructor(string memory _title, string memory _description, string[] memory _pros, string[] memory _cons) {
        chairman = msg.sender;
        registerForeman(msg.sender, 365);
    }

    function updateChairman(address newChairman) public managerOnly(msg.sender) {
        chairman = newChairman;
    }

    function updateVotingPower(uint _votingPowerAuthorized, uint _votingPowerNonAuthorized) public managerOnly(msg.sender) {
        votingPowerAuthorized = _votingPowerAuthorized;
        votingPowerNonAuthorized = _votingPowerNonAuthorized;
    }

    function registerForeman(address account, uint periodInDays) public managerOnly(msg.sender) {
        Foremen[account].validUntilTimeStamp = block.timestamp + (periodInDays * 86400);
    }

    function registerVoter(address account, uint periodInDays) public foremenOnly(msg.sender) {
        RegisteredVoters[account].validUntilTimeStamp = block.timestamp + (periodInDays * 86400);
    }
    
    function createProposal(string memory _title, string memory _description, string[] memory _pros, string[] memory _cons) public foremenOnly(msg.sender) {
        proposals.push(Proposal({title: _title, description: _description, pros: _pros, cons: _cons, forVotes: 0, againstVotes: 0, voteCount: 0, state: State.Open }));
    }

    function closeProposal(uint index) proposalExists(index) public {
        proposals[index].state = State.Closed;
    }

    function getProposal(uint index) public view proposalExists(index) returns (Proposal memory) {
        return proposals[index];
    }

    function getProposals() public view returns (Proposal[] memory) {
        return proposals;
    }

    function vote(uint index, bool forVotes) public proposalExists(index) {
        require(!hasVoted[index][msg.sender], "Already voted"); // check if user already voted
        bool validRegisteredVoter = RegisteredVoters[msg.sender].validUntilTimeStamp >= block.timestamp;

        uint votingPower;
        if (validRegisteredVoter) votingPower = votingPowerAuthorized;
        else votingPower = votingPowerNonAuthorized;
        require(votingPower > 0, "You have no voting power"); // check if the sender has voting power

        if (forVotes) proposals[index].forVotes += votingPower;
        if (!forVotes) proposals[index].againstVotes += votingPower;

        if (forVotes || !forVotes) {
            proposals[index].voteCount += 1;
            hasVoted[index][msg.sender] = true;
        }
    }
}
