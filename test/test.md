# Testing the Vote Smart Contract

- Only the manager can create a new proposal
- Everybody can see the list of proposals
- Voting should only be possible on existing proposals
- Everyone should be possible to up- or downvote
- Double voting should not be possible
- The result of a proposal can be draw
- The result of a proposal can be true
- The result of a proposal can be false
